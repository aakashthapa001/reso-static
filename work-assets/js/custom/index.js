(function($) {
  // Banner Slider
  if ($(".js-site-banner").length) {
    $(".js-site-banner").slick({
      rows: 0,
      infinite: false,
      draggable: false,
      arrows: false
    })
  }

  // Testimonial Slider
  if ($(".js-testimonial-slider").length) {
    $(".js-testimonial-slider").slick({
      rows: 0,
      nextArrow: $(".js-testimonial-slider-next"),
      prevArrow: $(".js-testimonial-slider-prev")
    })
  }

  // Sticky Header
  if ($(".js-site-header").length) {
    $(window).scroll(function() {
      const STICKCLASS = "js-stick"
      const banner = $(".js-site-banner")
      const bannerHeight = banner.outerHeight(true)
      const header = $(".js-site-header")
      const headerHeight = header.outerHeight(true)
      const scrollTop = $(window).scrollTop()

      if (scrollTop >= bannerHeight - headerHeight) {
        header.addClass(STICKCLASS);
      } else {
        header.removeClass(STICKCLASS);
      }
    })
  }

  // Vertical Slider
  if ($(".js-site-vertical-slider").length) {
    $(".js-site-vertical-slider").slick({
      arrows: false,
      dots: true,
      draggable: false,
      rows: 0,
      infinite: false,
      vertical: true,
      appendDots: $(".js-site-vertical-dots"),
      responsive: [
        {
          breakpoint: 801,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            vertical: false,
            draggable: true
          }
        }
      ]
    })
  }

  // Staff Slider
  if ($(".js-staff-slider").length) {
    $(".js-staff-slider").slick({
      rows: 0,
      slidesToShow: 3,
      nextArrow: $(".js-staff-slider-next"),
      prevArrow: $(".js-staff-slider-prev"),
      responsive: [
        {
          breakpoint: 801,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 601,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    })
  }

  // Site Accordion
  if ($(".js-site-accordion").length) {
    const accordionHeader = $(".js-accordion-header")
    const accordionBody = $(".js-accordion-body")

    accordionHeader.on("click", function(e) {
      e.preventDefault()
      const ACTIVE_CLASS = "js-active"
      const self = $(this)

      if (self.hasClass(ACTIVE_CLASS)) {
        self.removeClass(ACTIVE_CLASS)
        self.next().slideUp("fast")
      } else {
        accordionHeader.removeClass(ACTIVE_CLASS)
        accordionBody.slideUp("fast")

        self.addClass(ACTIVE_CLASS)
        self.next().slideDown("fast")
      }
    });
  }

  // Select Map
  if ($(".js-select-map-address").length) {
    const ACTIVECLASS = "js-map-active"
    const MAP_STYLES = [
      {
        elementType: "geometry",
        stylers: [
          {
            color: "#f5f5f5"
          }
        ]
      },
      {
        elementType: "labels.icon",
        stylers: [
          {
            visibility: "off"
          }
        ]
      },
      {
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#616161"
          }
        ]
      },
      {
        elementType: "labels.text.stroke",
        stylers: [
          {
            color: "#f5f5f5"
          }
        ]
      },
      {
        featureType: "administrative.land_parcel",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#bdbdbd"
          }
        ]
      },
      {
        featureType: "poi",
        elementType: "geometry",
        stylers: [
          {
            color: "#eeeeee"
          }
        ]
      },
      {
        featureType: "poi",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#757575"
          }
        ]
      },
      {
        featureType: "poi.park",
        elementType: "geometry",
        stylers: [
          {
            color: "#e5e5e5"
          }
        ]
      },
      {
        featureType: "poi.park",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#9e9e9e"
          }
        ]
      },
      {
        featureType: "road",
        elementType: "geometry",
        stylers: [
          {
            color: "#ffffff"
          }
        ]
      },
      {
        featureType: "road.arterial",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#757575"
          }
        ]
      },
      {
        featureType: "road.highway",
        elementType: "geometry",
        stylers: [
          {
            color: "#dadada"
          }
        ]
      },
      {
        featureType: "road.highway",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#616161"
          }
        ]
      },
      {
        featureType: "road.local",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#9e9e9e"
          }
        ]
      },
      {
        featureType: "transit.line",
        elementType: "geometry",
        stylers: [
          {
            color: "#e5e5e5"
          }
        ]
      },
      {
        featureType: "transit.station",
        elementType: "geometry",
        stylers: [
          {
            color: "#eeeeee"
          }
        ]
      },
      {
        featureType: "water",
        elementType: "geometry",
        stylers: [
          {
            color: "#c9c9c9"
          }
        ]
      },
      {
        featureType: "water",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#9e9e9e"
          }
        ]
      }
    ]

    let map
    let geocoder
    let marker

    const activeMap = $('.js-map-active')
    const activeLatitude = activeMap.data('latitute')
    const activeLongitude = activeMap.data('longitude')
    
    const initMap = (() => {
      const ICON = {
        url: "../reso-static/assets/images/icon-map-marker.svg", // url
        scaledSize: new google.maps.Size(38, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
      }

      let coordinates = {
        lat: activeLatitude,
        lng: activeLongitude
      }

      map = new google.maps.Map(document.getElementById('js-contact-map'), {
        zoom: 12,
        center: coordinates,
        styles: MAP_STYLES
      })

      marker = new google.maps.Marker({
        position: coordinates,
        icon: ICON,
        map
      })

      // Address Links
      const addressLink = $(".js-select-map-address")

      // change location on address click
      addressLink.on("click", function(e) {
        e.preventDefault()
        const self = $(this)
        const latitute = self.data("latitute")
        const longitude = self.data("longitude")

        $(".js-select-map-address").removeClass(ACTIVECLASS)
        self.addClass(ACTIVECLASS)

        changeLocation(latitute, longitude)
      });

      // Change location
      const changeLocation = (lat, lng) => {
        const newLatLng = new google.maps.LatLng(lat, lng)
        marker.setPosition(newLatLng)
        map.panTo(newLatLng)
      }
    })()
  }

  // Main Navigation
  if ($('.js-site-header').length) {
    const SUBMENU_TOGGLE_ICON = `
      <a href="javascript: void(0);" class="js-toggle-submenu px-15 flex justify-center items-center md:hidden c-main-nav__submenu__toggle">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.79 12.4" fill="#fff" width="15" class="relative"><path d="M10.9 12.4a1.5 1.5 0 0 1-1.06-.4L.44 2.56A1.5 1.5 0 0 1 2.56.44l8.34 8.33L19.23.44a1.5 1.5 0 0 1 2.12 2.12L12 12a1.5 1.5 0 0 1-1.1.4z"/></svg>
      </a>
    `

    const CLASSES = {
      active: 'js-active',
      header: '.js-site-header',
      toggleNav: '.js-toggle-nav',
      mainNav: '.js-header-main-nav',
      navItem: '.c-main-nav__item',
      navSubmenu: '.js-header-nav-submenu',
      toggleSubmenu: '.js-toggle-submenu'
    }

    const submenu = $(CLASSES.navSubmenu)
    
    submenu.each(function() {
      const self = $(this)
      const li = self.find('li')

      if (li.length > 0) {
        const closestLi = self.closest(CLASSES.navItem)
        closestLi.prepend(SUBMENU_TOGGLE_ICON)
      }
    })

    $(CLASSES.toggleSubmenu).on('click', function(e) {
      e.preventDefault()
      const self = $(this)
      const closestLi = self.closest(CLASSES.navItem)
      const submenu = closestLi.find(CLASSES.navSubmenu)

      self.toggleClass(CLASSES.active)
      submenu.slideToggle('fast')
    })

    // Hamburger
    $(CLASSES.toggleNav).on('click', function(e) {
      e.preventDefault()
      const self = $(this)

      self.toggleClass(CLASSES.active)
      $(CLASSES.header).toggleClass(CLASSES.active)
      $(CLASSES.mainNav).slideToggle('fast')
    })
  }

  // News List Filter
  if ($('.js-news-filter').length) {
    $('.js-news-filter-category').select2({
      placeholder: 'All Content',
      dropdownParent: $('.js-news-categoty-filter-wrap'),
      containerCssClass: 'c-news-list__filter__cselectwrap',
      dropdownCssClass: 'c-news-list__filter__cselectdropdown'
    })
  }
})(jQuery);
