var gulp = (gulp = require("gulp")),
  gutil = require("gulp-util"),
  sass = require("gulp-sass"),
  include = require("gulp-include"),
  eslint = require("gulp-eslint"),
  sourcemaps = require("gulp-sourcemaps"),
  concat = require("gulp-concat"),
  imagemin = require("gulp-imagemin"),
  plumber = require("gulp-plumber"),
  notify = require("gulp-notify"),
  uglify = require("gulp-uglify"),
  rename = require("gulp-rename"),
  pngquant = require("gulp-pngquant"),
  cache = require("gulp-cache"),
  size = require("gulp-size"),
  del = require("del"),
  runSequence = require("run-sequence"),
  prefix = require("gulp-autoprefixer"),
  order = require("gulp-order"),
  combineMq = require("gulp-combine-mq"),
  cleanCss = require("gulp-clean-css"),
  babel = require("gulp-babel"),
  postcss = require('gulp-postcss'),
  webpack = require('webpack'),
  webpackStream = require('webpack-stream'),
  webpackConfig = require('./webpack.config.js');

// Destination folder
var destFolder = "assets";

// Working folder
var workingFolder = "work-assets";

// SASS OUTPUT OPTION
var sassOptions = {
  outputStyle: "expanded"
};

// CSS VENDOR PREFIX OPTION
var prefixerOptions = {
  browsers: ["> 1%", "last 3 versions", "iOS >=7"]
};

////// Styles TASK ///////
gulp.task("styles", function() {
  var tailwindcss = require('tailwindcss');

  return gulp
    .src(`${workingFolder}/scss/**/*.scss`)
    .pipe(plumber(plumberErrorHandler))
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions))
    .pipe(postcss([
      tailwindcss('./tailwind.js'),
      require('autoprefixer')
    ]))
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(
      cleanCss({
        format: "beautify",
        level: {
          2: {
            removeUnusedAtRules: true
          }
        }
      })
    )
    .pipe(
      combineMq({
        beautify: false
      })
    )
    .pipe(concat("site.css"))
    .pipe(
      rename({
        //renames the concatenated CSS file
        basename: "site", //the base name of the renamed CSS file
        extname: ".min.css" //the extension fo the renamed CSS file
      })
    )
    .pipe(gulp.dest(`${destFolder}/css/`))
    .pipe(size({ gzip: true, showFiles: true }));
});

// JS LINT
gulp.task("lint", function() {
  return (
    gulp
      .src(`${workingFolder}/js/**`)
      .pipe(
        eslint({
          rules: {
            quotes: [1, "single"],
            semi: [1, "always"]
          }
        })
      )
      .pipe(eslint.format())
      // Brick on failure to be super strict
      .pipe(eslint.failOnError())
  );
});

/////////// ALL VENDOR Script TASK /////////////
gulp.task("vendors-scripts", function() {
  return gulp
    .src(`${workingFolder}/js/vendors/*.js`)
    .pipe(sourcemaps.init())
    .pipe(include())
    .pipe(order([

    ]))
    .pipe(concat("vendors.min.js"))
    .pipe(uglify())
    .on("error", function(err) {
      gutil.log(gutil.colors.red("[Error]"), err.toString());
    })
    .pipe(
      size({
        gzip: true,
        showFiles: true
      })
    )
    .pipe(gulp.dest(`${destFolder}/js/`));
});

/////////// CUSTOM Script TASK /////////////
gulp.task("scripts", function() {
  return gulp
    .src([
      `${workingFolder}/js/custom/index.js`
    ])
    .pipe(webpackStream(webpackConfig), webpack)
    .pipe(sourcemaps.init())
    .pipe(include())
    .on("error", console.log)
    .pipe(order([]))
    .pipe(concat("site.min.js"))
    .pipe(uglify())
    .pipe(
      size({
        gzip: true,
        showFiles: true
      })
    )
    .pipe(gulp.dest(`${destFolder}/js/`));
});

////// Optimize Image TASK ///////
gulp.task("img", function() {
  return gulp
    .src(`${workingFolder}/images/*.+(png|jpg|jpeg|gif|svg)`)
    .pipe(
      cache(
        imagemin({
          optimizationLevel: 7,
          interlaced: true,
          progressive: true,
          svgoPlugins: [{ removeViewBox: false }],
          use: [pngquant()]
        })
      )
    )
    .pipe(gulp.dest(`${destFolder}/images/`));
});

////// Copy Fonts TASK ///////
//Copy Fonts
gulp.task('fonts', function(){
  return gulp.src(`${workingFolder}/fonts/**/*`)
    .pipe(gulp.dest(`${destFolder}/fonts/`))
 });

// Cleaning
gulp.task("clean", function() {
  return del.sync(`${destFolder}`).then(function(cb) {
    return cache.clearAll(cb);
  });
});

gulp.task(`clean:${destFolder}`, function() {
  return del.sync([
    `${destFolder}/**/*`,
    `!${destFolder}/images`,
    `!${destFolder}/images/**/*`,
    `!${destFolder}/fonts`,
    `!${destFolder}/fonts/**/*`
  ]);
});

////// PLUMBER TASK ///////
var plumberErrorHandler = {
  errorHandler: notify.onError({
    title: "Gulp",
    message: "Error: <%= error.message %>"
  })
};

//Watch task
gulp.task(
  "watch",
  [
    "styles",
    "vendors-scripts",
    "img",
    "scripts",
  ],
  function() {
    gulp.watch(`${workingFolder}/scss/**/*.scss`, ["styles"]);
    gulp.watch(`${workingFolder}/js/vendors/*.js`, [
      "vendors-scripts"
    ]);
    gulp.watch(`${workingFolder}/js/custom/**/*.js`, [
      "scripts"
    ]);
    gulp.watch(`${workingFolder}/images/*.+(png|jpg|jpeg|gif|svg)`, ["img"]);
  }
);

gulp.task("default", function(callback) {
  runSequence(["styles", "watch", "img"], callback);
});

gulp.task("build", function(callback) {
  runSequence(
    `clean:${destFolder}`,
    [
      "styles",
      "vendors-scripts",
      "scripts",
      "img",
      "fonts"
    ],
    callback
  );
});
